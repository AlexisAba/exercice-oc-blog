export class Post {

    constructor(
        public id: number,
        public titre: string,
        public content: string,
        public loveIts: number,
        public createdAt: Date
    ){}
}