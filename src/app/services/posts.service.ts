import { Subject } from 'rxjs';
import { Post } from '../models/post.model';

export class PostsService {


    postSubject = new Subject<any[]>();

    posts = [
        {
          id: 0,
          titre: 'Hello world',
          content: 'Salut ça va ?',
          loveIts: 5,
          createdAt: new Date()
        },
        {
            id: 1,
            titre: 'Premier post',
            content: 'Comment vont ?',
            loveIts: 2,
            createdAt: new Date()
        },
        {
            id: 2,
            titre: 'Wesh alors',
            content: 'lel',
            loveIts: -26,
            createdAt: new Date()
        }
    ];

    emitPost() {
        this.postSubject.next(this.posts.slice());
    }

    addPost(post: Post){
        this.posts.push(post);
        this.emitPost();
    }

    deletePost(post: Post) {
        const postIndexToRemove = this.posts.findIndex(
            (postE1) => {
                if(postE1 === post) {
                return true;
                }
            }
        );
        this.posts.splice(postIndexToRemove, 1);
        this.emitPost();
    }

    lovePost(id: number){
        this.posts[id].loveIts++;
        this.emitPost();
    }

    dontLovePost(id: number){
        this.posts[id].loveIts--;
        this.emitPost();
    }

    getPostById(id: number) {
        const post = this.posts.find(
            (s) => {
                return s.id === id;
            }
        );
        return post;
    }
}